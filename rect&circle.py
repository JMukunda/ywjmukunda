from pygame.locals import*
import pygame
from random import randint
import random
from time import sleep
pygame.init()
screen=pygame.display.set_mode((640,480))
pygame.display.set_caption("Shapes!!")
green=(127,232,23)
blue=(59,185,255)
red=(255,0,0)
orange=(230,108,44)
yellow=(255,216,1)
purple=(137,59,255)
pink=(247,120,161)
brown=(111,78,55)
black=(0,0,0)
white=(255,255,255)
l=[red,orange,yellow,green,blue,purple,pink,brown,white,black]
while True:
    screen.fill((black))
    x=randint(1,100)
    y=randint(57,78)
    pygame.draw.circle(screen,random.choice(l),(320,240),100)
    pygame.draw.rect(screen,random.choice(l),(515,89,x,y))
    sleep(2)
    pygame.display.update()
    for event in pygame.event.get():
        if event.type==QUIT:
            pygame.quit()
            exit()
